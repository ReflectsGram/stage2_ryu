import { controls } from '../../constants/controls';

let crit_status='no';

export async function fight(firstFighter, secondFighter) {
  let status=false;
  let status1='no';
  let status2='no';
  let time_status1 = 10;
  let time_status2=10;
  let health1=firstFighter.health;
  let health2=secondFighter.health;
  let health1_indicator=100;
  let health2_indicator=100;
  setInterval(()=>{time_status1++; time_status2++}, 1000);

  function runOnKeys(func, ...codes) {
    let pressed = new Set();

    document.addEventListener('keydown', function (event) {
      pressed.add(event.code);

      for (let code of codes) { // все ли клавиши из набора нажаты?
        if (!pressed.has(code)) {
          return;
        }
      }
      pressed.clear();
      func();
    });

    document.addEventListener('keyup', function (event) {
      if (event.code==controls.PlayerOneBlock){
        status1='no';
      }
      if (event.code==controls.PlayerTwoBlock){
        status2='no'
      }
      pressed.delete(event.code);
    });
  }
    runOnKeys(() => {
      status1 = 'defender';
    }, controls.PlayerOneBlock);
    runOnKeys(() => {
      status2 = 'defender';
    }, controls.PlayerTwoBlock);

    runOnKeys(()=>{
      if (time_status1>=10) {
        if (status1 == 'no') {
          crit_status = 'crit';
          health2 -= getDamage(firstFighter,secondFighter);
          health2_indicator=(health2/secondFighter.health)*100;
          if(health2_indicator<0){health2_indicator=0}
          document.getElementById('right-fighter-indicator').style.width = `${health2_indicator}%`;
          time_status1 = 0;
        }
        }

    }, ...controls.PlayerOneCriticalHitCombination);

    runOnKeys(()=>{
      if (time_status2>=10) {
        if (status2 == 'no') {
          crit_status = 'crit';
          health1 -= getDamage(secondFighter,firstFighter);
          health1_indicator=(health1/firstFighter.health)*100;
          if(health1_indicator<0){health1_indicator=0}
          document.getElementById('left-fighter-indicator').style.width = `${health1_indicator}%`;
          time_status2=0;
        }
      }
    }, ...controls.PlayerTwoCriticalHitCombination);

    runOnKeys(()=>{
      if (status1=='no') {
        if (status2 == 'defender') {
           health2 -= getDamage(firstFighter,secondFighter);
          health2_indicator=(health2/secondFighter.health)*100
        } else {
          health2 -= getDamage(firstFighter,0);
          health2_indicator=(health2/secondFighter.health)*100
        }
        if(health2_indicator<0){
          health2_indicator=0
        }
        document.getElementById('right-fighter-indicator').style.width = `${health2_indicator}%`;

      }
    },controls.PlayerOneAttack);

    runOnKeys(()=>{
      if (status2 == 'no') {
        if (status1 == 'defender') {
          health1 -= getDamage(secondFighter,firstFighter);
          health1_indicator=(health1/firstFighter.health)*100;
        } else {
          health1 -= getDamage(secondFighter,0);
          health1_indicator=(health1/firstFighter.health)*100;
        }
        if(health1_indicator<0){health1_indicator=0}
        document.getElementById('left-fighter-indicator').style.width = `${health1_indicator}%`;

      }
    },controls.PlayerTwoAttack);

  return new Promise((resolve) => {
    let i = window.setInterval(() => {
      let b = health1;
      let q = health2;
      if (b<=0){
        window.clearInterval(i);
        resolve(1);
      }
      if (q <= 0) {
        window.clearInterval(i);
        resolve(0);
      }
    }, 0);

  // resolve the promise with the winner when fight is over
  });
}


export function getDamage(attacker, defender) {
  // return damage
  let attack=getHitPower(attacker);
  let defense=0;
  let damage = 0;

  if (defender==0)
  {
    defense=0
  }
  else
    {
    defense = getBlockPower(defender);
  }

  if(crit_status=='crit'){
    damage = 2*attack ;
  }
  else {
    damage = attack - defense;
  }
  if (damage<0){
    damage=0;
  }

  crit_status='no';
  return damage
}

export function getHitPower(fighter) {
  const attack =  fighter.attack;
  let criticalHitChance=Math.random()  + 1;
  let attack_power=attack*criticalHitChance;
  // return hit power
  return attack_power
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  // return block power
  let  dodgeChance = Math.random()  + 1;
  let block_power=defense*dodgeChance;
  return block_power
}
